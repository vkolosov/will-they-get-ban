class BansData {
  final bool communityBanned;
  final bool vacBanned;
  final int numberOfVacBans;
  final int daysSinceLastBan;
  final int numberOfGameBans;
  final String economyBan; // TODO: узнать тип данных при забаненном статусе.

  BansData({
    required this.communityBanned,
    required this.vacBanned,
    required this.numberOfVacBans,
    required this.daysSinceLastBan,
    required this.numberOfGameBans,
    required this.economyBan,
  });
}
