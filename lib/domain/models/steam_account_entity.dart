import 'package:will_they_get_ban/domain/models/bans_data.dart';

class SteamAccountEntity {
  final String steamId64;
  final String nickName;
  final String avatarUrl;
  final String profileUrl;
  final String? description;
  final DateTime dateAdded;
  final BansData bansData;

  SteamAccountEntity({
    required this.steamId64,
    required this.nickName,
    required this.avatarUrl,
    required this.profileUrl,
    required this.dateAdded,
    required this.bansData,
    this.description,
  });

  bool get isBanned {
    return bansData.economyBan != 'none' ||
        bansData.numberOfGameBans != 0 ||
        bansData.daysSinceLastBan != 0 ||
        bansData.vacBanned != false ||
        bansData.numberOfVacBans != 0 ||
        bansData.communityBanned != false;
  }
}
