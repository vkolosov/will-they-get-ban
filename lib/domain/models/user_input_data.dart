class UserInputData {
  final String steamId64;
  final String? description;
  final DateTime dateAdded;

  UserInputData({
    required this.steamId64,
    required this.description,
    required this.dateAdded,
  });
}
