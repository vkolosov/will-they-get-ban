import 'package:will_they_get_ban/data/local/models/local_model.dart';
import 'package:will_they_get_ban/domain/models/user_input_data.dart';

abstract class ILocalRepo {
  List<LocalModel> getAllLocalSteamAccounts();

  void addSteamAccount(UserInputData steamAccountEntity);

  void deleteSteamAccount(String steamId64);

  String? getSteamApiKey();

  void addSteamApiKey(String steamApiKey);
}
