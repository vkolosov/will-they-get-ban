import 'package:will_they_get_ban/domain/models/steam_account_entity.dart';

abstract class IMainRepo {
  Future<List<SteamAccountEntity>> getAllSteamAccounts();
}
