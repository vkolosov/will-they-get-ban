import 'package:will_they_get_ban/data/api/models/bans_data_model.dart';
import 'package:will_they_get_ban/data/api/models/steamapi_model.dart';

abstract class IApiRepo {
  Future<List<BansDataModel>> getBansData(List<String> steamIds64);

  Future<List<SteamApiModel>> getAccountsInfo(List<String> steamIds64);
}
