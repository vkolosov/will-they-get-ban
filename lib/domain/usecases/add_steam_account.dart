import 'package:will_they_get_ban/domain/models/user_input_data.dart';
import 'package:will_they_get_ban/domain/repositories/local_repo.dart';

class AddSteamAccount {
  final ILocalRepo localRepo;

  AddSteamAccount(this.localRepo);

  void call(UserInputData userInputData) {
    // Добавить стим акк.
    localRepo.addSteamAccount(userInputData);
  }
}
