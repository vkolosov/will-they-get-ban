import 'package:will_they_get_ban/domain/models/steam_account_entity.dart';
import 'package:will_they_get_ban/domain/repositories/main_repo.dart';

class GetAllSteamAccounts {
  final IMainRepo mainRepo;

  GetAllSteamAccounts(this.mainRepo);

  Future<List<SteamAccountEntity>> call() async {
    // Вернуть все аккаунты.
    return mainRepo.getAllSteamAccounts();
  }
}
