import 'package:will_they_get_ban/domain/repositories/local_repo.dart';

class DeleteSteamAccount {
  final ILocalRepo localRepo;

  DeleteSteamAccount(this.localRepo);

  void call(String steamId64) {
    // Удалить аккаунт.
    localRepo.deleteSteamAccount(steamId64);
  }
}
