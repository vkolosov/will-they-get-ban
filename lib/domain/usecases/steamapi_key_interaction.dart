import 'package:will_they_get_ban/data/local/services/hive_service.dart';

class SteamApiKeyInteraction {
  final HiveService hiveService;

  SteamApiKeyInteraction(this.hiveService);

  void save(String steamApiKey) {
    hiveService.saveApiKey(steamApiKey);
  }

  String? get() {
    return hiveService.getApiKey();
  }

  void delete() {
    hiveService.deleteApiKey();
  }
}
