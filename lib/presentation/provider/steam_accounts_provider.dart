import 'package:flutter/material.dart';
import 'package:will_they_get_ban/core/errors/no_steamapi_key_error.dart';
import 'package:will_they_get_ban/core/modules/add_module.dart';
import 'package:will_they_get_ban/core/modules/delete_module.dart';
import 'package:will_they_get_ban/core/modules/get_all_module.dart';
import 'package:will_they_get_ban/domain/models/steam_account_entity.dart';
import 'package:will_they_get_ban/domain/models/user_input_data.dart';
import 'package:will_they_get_ban/domain/usecases/add_steam_account.dart';
import 'package:will_they_get_ban/domain/usecases/delete_steam_account.dart';
import 'package:will_they_get_ban/domain/usecases/get_all_steam_accounts.dart';

enum Status {
  none,
  added,
  deleted,
  refreshed,
}

class SteamAccountsProvider with ChangeNotifier {
  var _steamAccounts = <SteamAccountEntity>[];
  var isLoading = false;
  var status = Status.none;
  NoSteamApiKeyError? error;

  List<SteamAccountEntity> get steamAccounts => _steamAccounts;

  List<SteamAccountEntity> get bannedSteamAccounts {
    return _steamAccounts.where((element) => element.isBanned == true).toList();
  }

  Future<void> getSteamAccounts() async {
    isLoading = true;
    error = null;
    notifyListeners();

    try {
      GetAllSteamAccounts getAllSteamAccounts = GetAllModule.call();
      _steamAccounts = await getAllSteamAccounts();

      isLoading = false;

      notifyListeners();
      await Future.delayed(const Duration(seconds: 1));
      status = Status.none;
    } on NoSteamApiKeyError catch (err) {
      error = err;
      notifyListeners();
    }
  }

  void addSteamAccount(UserInputData userInputData) {
    AddSteamAccount addSteamAccount = AddModule.call();
    addSteamAccount(userInputData);
    status = Status.added;
    getSteamAccounts();
  }

  void deleteSteamAccount(String steamId64) {
    DeleteSteamAccount deleteSteamAccount = DeleteModule.call();
    deleteSteamAccount(steamId64);
    status = Status.deleted;
    getSteamAccounts();
  }
}
