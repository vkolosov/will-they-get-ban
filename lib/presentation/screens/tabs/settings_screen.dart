import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:will_they_get_ban/core/colors.dart';
import 'package:will_they_get_ban/core/modules/steamapi_key_interaction_module.dart';
import 'package:will_they_get_ban/presentation/provider/steam_accounts_provider.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key? key, required this.selectPage}) : super(key: key);

  final Function selectPage;

  @override
  State<SettingsScreen> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  final _inputController = TextEditingController();
  final steamApiKeyObj = SteamApiKeyInteractionModule.call();

  void _submitSteamApiKey() {
    if (_inputController.text.isEmpty) {
      return;
    }

    final steamApiKey = _inputController.text;
    steamApiKeyObj.save(steamApiKey);
    context.read<SteamAccountsProvider>().getSteamAccounts();
    widget.selectPage(0);
  }

  void _deleteApiKey() {
    steamApiKeyObj.delete();
    context.read<SteamAccountsProvider>().getSteamAccounts();
    widget.selectPage(0);
  }

  @override
  Widget build(BuildContext context) {
    final apiKey = steamApiKeyObj.get();

    return apiKey != null
        ? Padding(
            padding: const EdgeInsets.only(top: 15.0),
            child: Align(
              alignment: Alignment.topCenter,
              child: Column(
                children: [
                  RichText(
                    text: TextSpan(
                      text: 'Ваш ключ: ',
                      style: Theme.of(context).textTheme.headline3,
                      children: [
                        TextSpan(
                          text: apiKey,
                          style:
                              Theme.of(context).textTheme.headline3!.copyWith(
                                    fontSize: 17,
                                    fontWeight: FontWeight.bold,
                                  ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  ElevatedButton(
                      onPressed: _deleteApiKey,
                      child: const Text('Удалить Steam Api ключ')),
                ],
              ),
            ),
          )
        : Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'Добавить SteamAPI ключ:',
                    style: Theme.of(context).textTheme.headline2!.copyWith(
                          color: ScColors.darkText,
                          fontWeight: FontWeight.bold,
                        ),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                TextField(
                  autofocus: true,
                  decoration: InputDecoration(
                    labelText: 'Steam API',
                    labelStyle: Theme.of(context).textTheme.headline3,
                  ),
                  controller: _inputController,
                ),
                const SizedBox(
                  height: 20,
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: ElevatedButton(
                    onPressed: _submitSteamApiKey,
                    child: const Text('Добавить'),
                  ),
                ),
              ],
            ),
          );
  }
}
