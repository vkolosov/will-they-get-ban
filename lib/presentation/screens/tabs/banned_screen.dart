import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:will_they_get_ban/presentation/provider/steam_accounts_provider.dart';
import 'package:will_they_get_ban/presentation/widgets/steam_item.dart';

class BannedScreen extends StatefulWidget {
  const BannedScreen({Key? key}) : super(key: key);

  @override
  State<BannedScreen> createState() => _BannedScreen();
}

class _BannedScreen extends State<BannedScreen> {
  @override
  Widget build(BuildContext context) {
    final steamAccountsProvider = context.watch<SteamAccountsProvider>();

    if (steamAccountsProvider.error != null) {
      return Center(
        child: Text(steamAccountsProvider.error!.message),
      );
    }

    return steamAccountsProvider.isLoading
        ? const Center(
            child:
                SpinKitWave(color: Colors.black45, type: SpinKitWaveType.end),
          )
        : RefreshIndicator(
            onRefresh: context.read<SteamAccountsProvider>().getSteamAccounts,
            child: ListView.builder(
              itemBuilder: (context, index) {
                return SteamItem(
                  key: ValueKey(
                    steamAccountsProvider.bannedSteamAccounts[index].steamId64,
                  ),
                  entity: steamAccountsProvider.bannedSteamAccounts[index],
                );
              },
              itemCount: steamAccountsProvider.bannedSteamAccounts.length,
            ),
          );
  }
}
