import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:will_they_get_ban/presentation/provider/steam_accounts_provider.dart';
import 'package:will_they_get_ban/presentation/widgets/steam_item.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  void showToast(String text) {
    Fluttertoast.showToast(
        msg: text,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 2,
        backgroundColor: Colors.greenAccent,
        textColor: Colors.black,
        fontSize: 16.0);
  }

  @override
  Widget build(BuildContext context) {
    final steamAccountsProvider = context.watch<SteamAccountsProvider>();

    if (steamAccountsProvider.error != null) {
      return Center(
        child: Text(steamAccountsProvider.error!.message),
      );
    }

    if (!steamAccountsProvider.isLoading) {
      if (steamAccountsProvider.status == Status.deleted) {
        showToast('Аккаунт удален');
      } else if (steamAccountsProvider.status == Status.added) {
        showToast('Аккаунт добавлен');
      }
    }

    return steamAccountsProvider.isLoading
        ? const Center(
            child:
                SpinKitWave(color: Colors.black45, type: SpinKitWaveType.end),
          )
        : RefreshIndicator(
            onRefresh: context.read<SteamAccountsProvider>().getSteamAccounts,
            child: ListView.builder(
              itemBuilder: (context, index) {
                return SteamItem(
                  key: ValueKey(
                      steamAccountsProvider.steamAccounts[index].steamId64),
                  entity: steamAccountsProvider.steamAccounts[index],
                );
              },
              itemCount: steamAccountsProvider.steamAccounts.length,
            ),
          );
  }
}
