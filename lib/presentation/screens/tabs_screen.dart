import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:will_they_get_ban/core/utils/utils.dart';
import 'package:will_they_get_ban/presentation/provider/steam_accounts_provider.dart';
import 'package:will_they_get_ban/presentation/screens/tabs/banned_screen.dart';
import 'package:will_they_get_ban/presentation/screens/tabs/home_screen.dart';
import 'package:will_they_get_ban/presentation/screens/tabs/settings_screen.dart';

class TabsScreen extends StatefulWidget {
  const TabsScreen({Key? key}) : super(key: key);

  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  int _selectedPageIndex = 0;
  late List<Map<String, Object>> _pages;

  void _selectPage(int index) async {
    setState(() {
      _selectedPageIndex = index;
    });
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      context.read<SteamAccountsProvider>().getSteamAccounts();
    });

    _pages = [
      {
        'page': const Home(),
        'title': 'Все аккаунты',
      },
      {
        'page': const BannedScreen(),
        'title': 'Забаненные аккаунты',
      },
      {
        'page': SettingsScreen(selectPage: _selectPage),
        'title': 'Настройки',
      },
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_pages[_selectedPageIndex]['title'] as String),
      ),
      body: _pages[_selectedPageIndex]['page'] as Widget,
      bottomNavigationBar: BottomNavigationBar(
        onTap: _selectPage,
        currentIndex: _selectedPageIndex,
        // type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            label: 'Все аккаунты',
            icon: const Icon(Icons.supervisor_account),
          ),
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            label: 'Забаненные',
            icon: const Icon(Icons.stop),
          ),
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            icon: const Icon(Icons.settings),
            label: 'Настройки',
          ),
        ],
      ),
      floatingActionButton: _selectedPageIndex == 2
          ? null
          : FloatingActionButton(
              child: const Icon(Icons.add),
              onPressed: () => startAddNewTransaction(context),
            ),
      resizeToAvoidBottomInset: false,
    );
  }
}
