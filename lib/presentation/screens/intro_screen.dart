import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:will_they_get_ban/core/colors.dart';
import 'package:will_they_get_ban/core/modules/hive_module.dart';
import 'package:will_they_get_ban/presentation/screens/tabs_screen.dart';

class IntroScreen extends StatefulWidget {
  const IntroScreen({Key? key}) : super(key: key);

  @override
  State<IntroScreen> createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen> {
  static const pageDecoration = PageDecoration(
    titleTextStyle: TextStyle(
      color: Colors.white,
      fontSize: 25,
    ),
  );

  final listPagesViewModel = [
    PageViewModel(
      bodyWidget: Image.asset(
        'assets/images/main_screen.png',
      ),
      title: 'Узнай, когда его забанят',
      decoration: pageDecoration,
    ),
    PageViewModel(
      bodyWidget: Image.asset(
        'assets/images/settings_screen.png',
      ),
      title: 'Введи свой Steam API ключ',
      decoration: pageDecoration,
    ),
    PageViewModel(
      bodyWidget: Image.asset(
        'assets/images/add_new_screen.png',
      ),
      title: 'Добавь аккаунт и получи уведомление, когда его заблокируют',
      decoration: pageDecoration,
    ),
    PageViewModel(
      bodyWidget: Image.asset(
        'assets/images/main_screen_refresh.png',
      ),
      title: 'Обновляй сам или включи автоматическое обновление',
      decoration: pageDecoration,
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return IntroductionScreen(
      globalBackgroundColor: ScColors.bottomBarBg,
      isTopSafeArea: true,
      pages: listPagesViewModel,
      onDone: () {
        Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (_) => const TabsScreen()),
        );
        HiveModule.call().introHasViewed();
      },
      onSkip: () {
        Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (_) => const TabsScreen()),
        );
        HiveModule.call().introHasViewed();
      },
      showBackButton: false,
      showSkipButton: true,
      skip: const Text(
        'Skip',
        style: TextStyle(
          fontSize: 18,
          fontWeight: FontWeight.bold,
        ),
      ),
      next: const Text(
        'Next',
        style: TextStyle(
          fontSize: 18,
          fontWeight: FontWeight.bold,
        ),
      ),
      done: const Text(
        'Done',
        style: TextStyle(
          fontSize: 18,
          fontWeight: FontWeight.bold,
        ),
      ),
      dotsDecorator: DotsDecorator(
        size: const Size.square(10.0),
        activeSize: const Size(20.0, 10.0),
        activeColor: Theme.of(context).colorScheme.secondary,
        color: Colors.black26,
        spacing: const EdgeInsets.symmetric(horizontal: 3.0),
        activeShape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25.0),
        ),
      ),
    );
  }
}
