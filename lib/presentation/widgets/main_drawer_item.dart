import 'package:flutter/material.dart';

class MainDrawerItem extends StatelessWidget {
  final Icon icon;
  final String title;
  final VoidCallback onTapCallBack;

  const MainDrawerItem(
      {Key? key,
      required this.icon,
      required this.title,
      required this.onTapCallBack})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: icon,
      title: Text(
        title,
        style: const TextStyle(fontSize: 20),
      ),
      onTap: onTapCallBack,
    );
  }
}
