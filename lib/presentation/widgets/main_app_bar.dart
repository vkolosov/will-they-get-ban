import 'package:flutter/material.dart';
import 'package:will_they_get_ban/core/utils/utils.dart';

class MainAppBar extends AppBar {
  MainAppBar({Key? key}) : super(key: key);

  Widget build(BuildContext context) {
    return AppBar(
      title: const Text('Main Page'),
      actions: [
        IconButton(
          icon: const Icon(Icons.add),
          onPressed: () => startAddNewTransaction(context),
        ),
      ],
    );
  }
}
