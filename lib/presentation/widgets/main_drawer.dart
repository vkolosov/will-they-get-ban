import 'package:flutter/material.dart';
import 'package:will_they_get_ban/presentation/widgets/main_drawer_item.dart';

class MainDrawer extends StatelessWidget {
  const MainDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          Container(
            height: 120,
            width: double.infinity,
            padding: const EdgeInsets.all(20),
            alignment: Alignment.centerLeft,
            color: const Color.fromRGBO(121, 202, 68, 0.5254901960784314),
            child: Text(
              'Меню',
              style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 30,
                  color: Theme.of(context).primaryColor),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          MainDrawerItem(
            icon: const Icon(Icons.arrow_forward_ios_rounded),
            title: 'Полный список',
            onTapCallBack: () {},
          ),
          MainDrawerItem(
            icon: const Icon(Icons.arrow_forward_ios_rounded),
            title: 'Забаненые',
            onTapCallBack: () {},
          ),
        ],
      ),
    );
  }
}
