import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:will_they_get_ban/domain/models/steam_account_entity.dart';
import 'package:will_they_get_ban/presentation/provider/steam_accounts_provider.dart';

class SteamItem extends StatelessWidget {
  final SteamAccountEntity entity;

  const SteamItem({
    Key? key,
    required this.entity,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: entity.isBanned ? Colors.red.withOpacity(0.6) : null,
      child: ListTile(
        leading: CircleAvatar(
          backgroundImage: NetworkImage(entity.avatarUrl),
        ),
        title: Text(
          entity.nickName,
          style: Theme.of(context).textTheme.headline3!.copyWith(
                fontWeight: FontWeight.bold,
              ),
        ),
        subtitle: Text(
          entity.description ?? '',
          style: Theme.of(context).textTheme.headline3,
        ),
        trailing: IconButton(
          icon: const Icon(Icons.delete),
          onPressed: () => context
              .read<SteamAccountsProvider>()
              .deleteSteamAccount(entity.steamId64),
        ),
      ),
    );
  }
}
