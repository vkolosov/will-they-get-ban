import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:will_they_get_ban/core/colors.dart';
import 'package:will_they_get_ban/domain/models/user_input_data.dart';
import 'package:will_they_get_ban/presentation/provider/steam_accounts_provider.dart';

class AddNewSteam extends StatefulWidget {
  const AddNewSteam({Key? key}) : super(key: key);

  @override
  State<AddNewSteam> createState() => _AddNewSteamState();
}

class _AddNewSteamState extends State<AddNewSteam> {
  final _form = GlobalKey<FormState>();

  late final String _steamId64;

  late final String? _description;

  void submitForm(BuildContext context) {
    final isValid = _form.currentState!.validate();
    if (!isValid) {
      return;
    }

    _form.currentState!.save();

    UserInputData steamAccount = UserInputData(
        steamId64: _steamId64,
        description: _description,
        dateAdded: DateTime.now());

    context.read<SteamAccountsProvider>().addSteamAccount(steamAccount);
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          decoration: const BoxDecoration(
            color: ScColors.appBarBg,
          ),
          height: 60,
          alignment: Alignment.center,
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                decoration: BoxDecoration(
                  color: Colors.grey,
                  borderRadius: BorderRadius.circular(10),
                ),
                width: 40,
                height: 3,
                margin: const EdgeInsets.only(top: 6.0, bottom: 5.0),
              ),
              Text(
                'Добавить аккаунт',
                style: Theme.of(context).textTheme.headline2,
              ),
            ],
          ),
        ),
        Padding(
          padding:
              const EdgeInsets.only(top: 20, right: 10, left: 10, bottom: 10),
          child: Form(
            key: _form,
            child: Column(
              children: [
                const SizedBox(
                  height: 20,
                ),
                TextFormField(
                  autofocus: true,
                  keyboardType: TextInputType.number,
                  textInputAction: TextInputAction.next,
                  decoration: const InputDecoration(
                    labelText: 'SteamID64',
                    hintText: '76500000000000000',
                  ),
                  inputFormatters: [
                    LengthLimitingTextInputFormatter(17),
                    FilteringTextInputFormatter.digitsOnly,
                  ],
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Введите SteamId64';
                    }
                    return null;
                  },
                  onSaved: (value) {
                    if (value != null) {
                      _steamId64 = value;
                    }
                  },
                ),
                TextFormField(
                  maxLines: 3,
                  decoration: const InputDecoration(labelText: 'Description'),
                  onSaved: (value) {
                    if (value != null) {
                      _description = value;
                    }
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: ElevatedButton(
                    onPressed: () => submitForm(context),
                    child: const Text('Добавить'),
                  ),
                ),
              ],
              mainAxisSize: MainAxisSize.max,
            ),
          ),
        ),
      ],
    );
  }
}
