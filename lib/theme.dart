import 'package:flutter/material.dart';
import 'package:will_they_get_ban/core/colors.dart';

final scTheme = ThemeData(
  fontFamily: 'Quicksand',
  textButtonTheme: const TextButtonThemeData(style: ButtonStyle()),
  textTheme: const TextTheme(
    button: TextStyle(
      color: ScColors.lightText,
      // fontWeight: FontWeight.bold,
      fontSize: 15,
    ),
    headline2: TextStyle(
      color: ScColors.lightText,
      fontSize: 20,
    ),
    headline3: TextStyle(
      color: ScColors.darkText,
      fontSize: 15,
    ),
  ),
  appBarTheme: const AppBarTheme(
    backgroundColor: ScColors.appBarBg,
  ),
  bottomNavigationBarTheme: const BottomNavigationBarThemeData(
    backgroundColor: ScColors.bottomBarBg,
    unselectedItemColor: Colors.grey,
    selectedItemColor: Colors.white,
  ),
  floatingActionButtonTheme: const FloatingActionButtonThemeData(
    backgroundColor: Colors.lightGreen,
  ),
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ButtonStyle(
      shape: MaterialStateProperty.all(
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(6))),
      backgroundColor: MaterialStateProperty.all(ScColors.appBarBg),
    ),
  ),
  cardTheme: CardTheme(
    color: ScColors.cardBg,
    elevation: 5.0,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
  ),
  inputDecorationTheme: const InputDecorationTheme(
    labelStyle: TextStyle(
      color: ScColors.bottomBarBg,
    ),
  ),
  scaffoldBackgroundColor: Colors.white,
  primaryColor: const Color.fromRGBO(84, 66, 103, 1.0),
  backgroundColor: Colors.red,
);
