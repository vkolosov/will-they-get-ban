class BansDataModel {
  final String steamId64;
  final bool communityBanned;
  final bool vacBanned;
  final int numberOfVacBans;
  final int daysSinceLastBan;
  final int numberOfGameBans;
  final String economyBan;

  BansDataModel(
    this.steamId64,
    this.communityBanned,
    this.vacBanned,
    this.numberOfVacBans,
    this.daysSinceLastBan,
    this.numberOfGameBans,
    this.economyBan,
  );

  BansDataModel.fromJson(Map<String, dynamic> json)
      : steamId64 = json['SteamId'],
        communityBanned = json['CommunityBanned'],
        vacBanned = json['VACBanned'],
        numberOfVacBans = json['NumberOfVACBans'],
        daysSinceLastBan = json['DaysSinceLastBan'],
        numberOfGameBans = json['NumberOfGameBans'],
        economyBan = json['EconomyBan'];
}
