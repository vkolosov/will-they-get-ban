class SteamApiModel {
  final String steamId64;
  final String nickName;
  final String avatarUrl;
  final String profileUrl;

  SteamApiModel(
    this.steamId64,
    this.nickName,
    this.avatarUrl,
    this.profileUrl,
  );

  SteamApiModel.fromJson(Map<String, dynamic> json)
      : steamId64 = json['steamid'],
        nickName = json['personaname'],
        avatarUrl = json['avatar'],
        profileUrl = json['profileurl'];
}
