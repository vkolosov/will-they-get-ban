import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:will_they_get_ban/data/api/models/bans_data_model.dart';
import 'package:will_they_get_ban/data/api/models/steamapi_model.dart';

class SteamApi {
  final String apiKey;

  SteamApi({required this.apiKey});

  String get _getBansUrl =>
      'https://api.steampowered.com/ISteamUser/GetPlayerBans/v1?key=$apiKey&steamids=';

  String get _getAccountsInfoUrl =>
      'https://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=$apiKey&steamids=';

  Future<List<BansDataModel>> getBansInfo(List<String> steamIds64) async {
    final response =
        await http.get(Uri.parse('$_getBansUrl${steamIds64.join(',')}'));

    final body = json.decode(response.body);
    return (body['players'] as List)
        .map((e) => BansDataModel.fromJson(e))
        .toList();
  }

  Future<List<SteamApiModel>> getAccountsInfo(List<String> steamIds64) async {
    final response = await http
        .get(Uri.parse('$_getAccountsInfoUrl${steamIds64.join(',')}'));

    final body = json.decode(response.body);
    return (body['response']['players'] as List)
        .map((e) => SteamApiModel.fromJson(e))
        .toList();
  }
}
