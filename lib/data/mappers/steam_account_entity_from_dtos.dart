import 'package:will_they_get_ban/data/api/models/bans_data_model.dart';
import 'package:will_they_get_ban/data/api/models/steamapi_model.dart';
import 'package:will_they_get_ban/data/local/models/local_model.dart';
import 'package:will_they_get_ban/data/mappers/bans_data_entity_from_dto.dart';
import 'package:will_they_get_ban/domain/models/steam_account_entity.dart';

class SteamAccountEntityFromDtos {
  static SteamAccountEntity call({
    required LocalModel localDto,
    required SteamApiModel steamApiDto,
    required BansDataModel bansDataDto,
  }) {
    return SteamAccountEntity(
      steamId64: localDto.steamId64,
      nickName: steamApiDto.nickName,
      description: localDto.description,
      avatarUrl: steamApiDto.avatarUrl,
      profileUrl: steamApiDto.profileUrl,
      dateAdded: localDto.dateAdded,
      bansData: BansDataEntityFromDto.call(bansDataDto),
    );
  }
}
