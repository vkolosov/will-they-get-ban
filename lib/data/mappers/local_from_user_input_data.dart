import 'package:will_they_get_ban/data/local/models/local_model.dart';
import 'package:will_they_get_ban/domain/models/user_input_data.dart';

class LocalFromUserInputData {
  static LocalModel call(UserInputData userInputData) {
    return LocalModel(
      steamId64: userInputData.steamId64,
      dateAdded: userInputData.dateAdded,
      description: userInputData.description,
    );
  }
}
