import 'package:will_they_get_ban/data/api/models/bans_data_model.dart';
import 'package:will_they_get_ban/domain/models/bans_data.dart';

class BansDataEntityFromDto {
  static BansData call(BansDataModel bansDataDto) {
    return BansData(
      communityBanned: bansDataDto.communityBanned,
      vacBanned: bansDataDto.vacBanned,
      numberOfVacBans: bansDataDto.numberOfVacBans,
      daysSinceLastBan: bansDataDto.daysSinceLastBan,
      numberOfGameBans: bansDataDto.numberOfGameBans,
      economyBan: bansDataDto.economyBan,
    );
  }
}
