import 'package:hive_flutter/hive_flutter.dart';
import 'package:will_they_get_ban/data/local/models/local_model.dart';

class HiveService {
  Box<LocalModel> accountsBox = Hive.box<LocalModel>('SteamStorage');
  Box<String> apiKeyBox = Hive.box<String>('ApiKey');

  List<LocalModel> getSteamAccounts() {
    return accountsBox.values.toList();
  }

  void addSteamAccount(LocalModel steamAccount) {
    accountsBox.put(steamAccount.steamId64, steamAccount);
  }

  void deleteSteamAccount(String steamId64) {
    accountsBox.delete(steamId64);
  }

  void saveApiKey(String apiKey) {
    apiKeyBox.put('ApiKey', apiKey);
  }

  String? getApiKey() {
    return apiKeyBox.get('ApiKey');
  }

  void deleteApiKey() {
    apiKeyBox.delete('ApiKey');
  }

  void introHasViewed() {
    apiKeyBox.put('intro', 'viewed');
  }

  bool isIntroViewed() {
    return apiKeyBox.get('intro') != null;
  }
}
