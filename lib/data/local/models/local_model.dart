import 'package:hive/hive.dart';

part 'local_model.g.dart';

@HiveType(typeId: 4)
class LocalModel {
  @HiveField(0)
  final String steamId64;

  @HiveField(1)
  final String? description;

  @HiveField(2)
  final DateTime dateAdded;

  LocalModel({
    required this.steamId64,
    required this.description,
    required this.dateAdded,
  });
}
