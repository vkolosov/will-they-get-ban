import 'package:will_they_get_ban/data/local/models/local_model.dart';
import 'package:will_they_get_ban/data/local/services/hive_service.dart';
import 'package:will_they_get_ban/data/mappers/local_from_user_input_data.dart';
import 'package:will_they_get_ban/domain/models/user_input_data.dart';
import 'package:will_they_get_ban/domain/repositories/local_repo.dart';

class LocalRepoImpl implements ILocalRepo {
  final HiveService hiveService;

  LocalRepoImpl(this.hiveService);

  @override
  void addSteamAccount(UserInputData userInputData) {
    hiveService.addSteamAccount(LocalFromUserInputData.call(userInputData));
  }

  @override
  void deleteSteamAccount(String steamId64) {
    hiveService.deleteSteamAccount(steamId64);
  }

  @override
  List<LocalModel> getAllLocalSteamAccounts() {
    return hiveService.getSteamAccounts();
  }

  @override
  void addSteamApiKey(String steamApiKey) {
    hiveService.saveApiKey(steamApiKey);
  }

  @override
  String? getSteamApiKey() {
    return hiveService.getApiKey();
  }
}
