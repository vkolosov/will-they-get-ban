import 'package:will_they_get_ban/data/api/models/bans_data_model.dart';
import 'package:will_they_get_ban/data/api/models/steamapi_model.dart';
import 'package:will_they_get_ban/data/api/services/steamapi_service.dart';
import 'package:will_they_get_ban/domain/repositories/api_repo.dart';

class ApiRepoImpl implements IApiRepo {
  final SteamApi steamApi;

  ApiRepoImpl(this.steamApi);

  @override
  Future<List<SteamApiModel>> getAccountsInfo(List<String> steamIds64) async {
    return await steamApi.getAccountsInfo(steamIds64);
  }

  @override
  Future<List<BansDataModel>> getBansData(List<String> steamIds64) async {
    return await steamApi.getBansInfo(steamIds64);
  }
}
