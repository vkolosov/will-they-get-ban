import 'package:collection/collection.dart';

import 'package:will_they_get_ban/core/modules/hive_module.dart';
import 'package:will_they_get_ban/core/modules/steamapi_module.dart';
import 'package:will_they_get_ban/data/api/models/bans_data_model.dart';
import 'package:will_they_get_ban/data/api/models/steamapi_model.dart';
import 'package:will_they_get_ban/data/local/models/local_model.dart';
import 'package:will_they_get_ban/data/mappers/steam_account_entity_from_dtos.dart';
import 'package:will_they_get_ban/data/repositories/api_repo_impl.dart';
import 'package:will_they_get_ban/data/repositories/local_repo_impl.dart';
import 'package:will_they_get_ban/domain/models/steam_account_entity.dart';
import 'package:will_they_get_ban/domain/repositories/main_repo.dart';

class MainRepoImpl implements IMainRepo {
  final LocalRepoImpl localRepo = LocalRepoImpl(HiveModule.call());
  final ApiRepoImpl apiRepo = ApiRepoImpl(SteamApiModule.get());

  @override
  Future<List<SteamAccountEntity>> getAllSteamAccounts() async {
    final steamAccountEntities = <SteamAccountEntity>[];

    List<LocalModel> localSteamAccounts = localRepo.getAllLocalSteamAccounts();
    List<String> steamIds64 =
        localSteamAccounts.map((e) => e.steamId64).toList();
    final steamApiAccountsInfo = await apiRepo.getAccountsInfo(steamIds64);
    final bansData = await apiRepo.getBansData(steamIds64);

    for (var el in localSteamAccounts) {
      SteamApiModel? steamApiModel = steamApiAccountsInfo.firstWhereOrNull(
        (element) => element.steamId64 == el.steamId64,
      );
      BansDataModel? bansDataModel = bansData.firstWhereOrNull(
        (element) => element.steamId64 == el.steamId64,
      );

      if (steamApiModel != null && bansDataModel != null) {
        steamAccountEntities.add(
          SteamAccountEntityFromDtos.call(
            localDto: el,
            steamApiDto: steamApiModel,
            bansDataDto: bansDataModel,
          ),
        );
      }
    }
    return steamAccountEntities;
  }
}
