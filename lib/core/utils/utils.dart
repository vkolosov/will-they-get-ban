import 'package:flutter/material.dart';
import 'package:will_they_get_ban/presentation/widgets/add_new_steam.dart';

void startAddNewTransaction(BuildContext context) {
  showModalBottomSheet(
    clipBehavior: Clip.antiAlias,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
    constraints: BoxConstraints.tight(
      Size(
          double.infinity,
          MediaQuery.of(context).size.height -
              AppBar().preferredSize.height -
              MediaQuery.of(context).padding.top),
    ),
    isScrollControlled: true,
    context: context,
    builder: (_) {
      return const AddNewSteam();
    },
  );
}
