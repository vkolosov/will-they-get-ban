import 'package:hive_flutter/hive_flutter.dart';
import 'package:will_they_get_ban/data/local/models/local_model.dart';

class HiveInit {
  static Future<void> init() async {
    await Hive.initFlutter();
    Hive.registerAdapter(LocalModelAdapter());
    await Hive.openBox<LocalModel>('SteamStorage');
    await Hive.openBox<String>('ApiKey');
  }
}
