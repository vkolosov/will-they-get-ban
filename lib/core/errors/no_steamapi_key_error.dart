class NoSteamApiKeyError extends Error {
  final String message;

  NoSteamApiKeyError(this.message);

  @override
  String toString() {
    return message;
  }
}
