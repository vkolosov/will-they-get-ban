import 'package:will_they_get_ban/core/modules/hive_module.dart';
import 'package:will_they_get_ban/domain/usecases/steamapi_key_interaction.dart';

class SteamApiKeyInteractionModule {
  static SteamApiKeyInteraction call() {
    return SteamApiKeyInteraction(
      HiveModule.call(),
    );
  }
}
