import 'package:will_they_get_ban/data/local/services/hive_service.dart';

class HiveModule {
  static HiveService? _hiveService;

  static HiveService call() {
    _hiveService ??= HiveService();
    return _hiveService!;
  }
}
