import 'package:will_they_get_ban/core/modules/hive_module.dart';
import 'package:will_they_get_ban/data/repositories/local_repo_impl.dart';
import 'package:will_they_get_ban/domain/usecases/add_steam_account.dart';

class AddModule {
  static AddSteamAccount call() {
    return AddSteamAccount(
      LocalRepoImpl(
        HiveModule.call(),
      ),
    );
  }
}
