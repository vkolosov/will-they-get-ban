import 'package:will_they_get_ban/core/errors/no_steamapi_key_error.dart';
import 'package:will_they_get_ban/data/api/services/steamapi_service.dart';
import 'package:will_they_get_ban/data/local/services/hive_service.dart';

class SteamApiModule {
  static SteamApi get() {
    final steamApiKey = HiveService().getApiKey();
    if (steamApiKey == null) {
      throw NoSteamApiKeyError('Нет SteamApi Key');
    }
    return SteamApi(apiKey: steamApiKey);
  }
}
