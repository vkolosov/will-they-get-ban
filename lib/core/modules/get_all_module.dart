import 'package:will_they_get_ban/data/repositories/main_repo_impl.dart';
import 'package:will_they_get_ban/domain/usecases/get_all_steam_accounts.dart';

class GetAllModule {
  static GetAllSteamAccounts call() {
    return GetAllSteamAccounts(
      MainRepoImpl(),
    );
  }
}
