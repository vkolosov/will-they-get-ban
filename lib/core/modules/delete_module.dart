import 'package:will_they_get_ban/core/modules/hive_module.dart';
import 'package:will_they_get_ban/data/repositories/local_repo_impl.dart';
import 'package:will_they_get_ban/domain/usecases/delete_steam_account.dart';

class DeleteModule {
  static DeleteSteamAccount call() {
    return DeleteSteamAccount(
      LocalRepoImpl(
        HiveModule.call(),
      ),
    );
  }
}
