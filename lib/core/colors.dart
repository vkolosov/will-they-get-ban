import 'package:flutter/material.dart';

class ScColors {
  static const cardBg = Color.fromRGBO(214, 210, 213, 1.0);
  static const appBarBg = Color.fromRGBO(186, 39, 94, 1.0);
  static const bottomBarBg = Color.fromRGBO(144, 16, 64, 1.0);

  static const lightText = Colors.white;
  static const darkText = bottomBarBg;
}
