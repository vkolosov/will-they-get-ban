import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:will_they_get_ban/core/modules/hive_module.dart';
import 'package:will_they_get_ban/core/utils/hive_init.dart';
import 'package:will_they_get_ban/presentation/provider/steam_accounts_provider.dart';
import 'package:will_they_get_ban/presentation/screens/tabs_screen.dart';
import 'package:will_they_get_ban/presentation/screens/intro_screen.dart';
import 'package:will_they_get_ban/theme.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await HiveInit.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final isIntroViewed = HiveModule.call().isIntroViewed();

    return ChangeNotifierProvider(
      create: (BuildContext context) => SteamAccountsProvider(),
      child: MaterialApp(
          title: 'Will they get ban?',
          home: isIntroViewed ? const TabsScreen() : const IntroScreen(),
          theme: scTheme,
          routes: {
            '/home': (context) => const TabsScreen(),
          }),
    );
  }
}
